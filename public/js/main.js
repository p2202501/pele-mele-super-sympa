var select = null;

function clickCell(cell) {
  const retour = cell.classList.toggle("selected");
  return retour;
}

function fetchImages(recherche) {
  const token = "cAWG0PnIt8M8EuILoUH25xiKKLDPGxXPnOU5MxnuhjWpjFl3YxZu6S6S";
  const url = `https://api.pexels.com/v1/search?query=${recherche ? recherche : "people"}&orientation=square&per_page=6`;
  return fetch(url, {
    headers: {
      Authorization: token
    }
  })
    .then(response => response.json())
    .then(data => {
      let images = [];
      for (let image of data.photos) {
        images.push(image.src.original);
      }
      return images;
    });
}

function onSelect(cell) {
  // selectionne la zone si elle ne l'etait pas, sinon la deselectionne
  let isSelect = clickCell(cell);
  if (isSelect) { // si elle vient d'etre selectionnee
    if (select !== null) { // s'il y a deja une selection, ça lui enleve son bord rouge
      select.classList.toggle("selected");
    }
    select = cell; // met la div actuelle dans la variable select
  } else { // si elle vient d'etre deselectionnee
    select = null;
  }
}

function onImageClick(image) {
  // ajoute l'image à la div de selection s'il y en a une
  if (select !== null) {
    select.innerHTML = "";
    const nodeImage = document.createElement("img");
    nodeImage.src = image.src;
    select.appendChild(nodeImage);
  }
}

function onUnselect() {
  // deselectionne la selection s'il y en a une
  if (select !== null) {
    select.classList.toggle("selected");
  }
  select = null;
}

function clickMain(event) {
  if (event.target.tagName === "MAIN") {
    onUnselect();
  } else if (event.target.classList[0] === "image-cell") {
    onSelect(event.target);
  } else if (event.target.tagName === "IMG") {
    onSelect(event.target.parentNode);
  }
}

function clickAside(event) {
  if (event.target.tagName === "IMG") {
    onImageClick(event.target);
  } else {
    onUnselect();
  }
}

async function afficherImages(recherche) {
  const divImages = document.getElementsByClassName("select-images")[0]; // div à gauche
  divImages.innerHTML = "";
  try {
    const urlsImages = await fetchImages(recherche); // récupère liste de 6 urls d'images
    if (urlsImages.length === 0) {
      throw Error("Aucune image trouvée");
    }

    // créé des images avec les urls et les met dans la div à gauche
    for (let url of urlsImages) {
      let nodeImage = document.createElement("img");
      nodeImage.src = url;
      nodeImage.classList.add("image-gauche");
      divImages.appendChild(nodeImage);
    }
  } catch (e) {
    divImages.innerHTML = "<p>" + e + "</p>";
  }
}

async function main() {
  afficherImages("people");

  const inputRecherche = document.getElementById("recherche");
  inputRecherche.addEventListener("change", () => {
    afficherImages(inputRecherche.value);
  });

  let asides = document.getElementsByTagName("aside");
  for (let aside of asides) {
    aside.addEventListener("click", clickAside);
  }
  document.getElementsByTagName("main")[0].addEventListener("click", clickMain);
}

main();